from common.json import ModelEncoder

from .models import AutomobileVO, Customer, Sale, Salesperson


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "import_href", "sold"]


class CustomerModelEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name", "last_name", "address", "phone_number", "id"]


class SalespersonModelEncoder(ModelEncoder):
    model = Salesperson
    properties = ["first_name", "last_name", "employee_id", "pk", "id"]


class SaleModelEncoder(ModelEncoder):
    model = Sale
    properties = ["automobile", "salesperson", "customer", "price", "id"]

    encoders = {
        "automobile": AutomobileVODetailEncoder(),
        "salesperson": SalespersonModelEncoder(),
        "customer": CustomerModelEncoder(),
    }
