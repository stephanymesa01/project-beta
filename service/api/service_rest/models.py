from django.db import models
from django.urls import reverse


class Technician(models.Model):
    first_name = models.CharField(max_length=100, null=True, default="Unknown")
    last_name = models.CharField(max_length=100, null=True, default="Unknown")
    employee_id = models.CharField(
        max_length=100, null=True, default="Unknown")

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    def get_api_url(self):
        return reverse("api_list_technicians", kwargs={"pk": self.id})


class Appointment(models.Model):
    class Status(models.TextChoices):
        SCHEDULED = "scheduled", "Scheduled"
        CANCELED = "canceled", "Canceled"
        FINISHED = "finished", "Finished"

    customer_name = models.CharField(
        max_length=200, default="Default Customer")
    vin = models.CharField(max_length=300)
    vip = models.BooleanField(default=False)
    date = models.DateField(null=True)
    time = models.TimeField(null=True)
    technician = models.ForeignKey(
        "Technician", related_name="appointments", on_delete=models.PROTECT
    )
    reason = models.TextField()
    status = models.CharField(
        max_length=10, choices=Status.choices, default=Status.SCHEDULED
    )

    def __str__(self):
        return f"{self.customer_name} {self.vin}"

    def cancel(self):
        self.status = "Canceled"

    def finish(self):
        self.status = "Finished"


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, null=True)
    vin = models.CharField(max_length=300, unique=True)
    sold = models.BooleanField(default=False)
