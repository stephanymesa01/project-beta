import React from "react";
import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          CarCar
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">
                Home
              </NavLink>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="/#"
                id="navbarDropdownService"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Service
              </a>
              <ul
                className="dropdown-menu"
                aria-labelledby="navbarDropdownService"
              >
                <li className="dropdown-header">Technician</li>
                <li>
                  <NavLink className="dropdown-item" to="/technicians">
                    All Technicians
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/technicians/new">
                    Add Technician
                  </NavLink>
                </li>
                <li className="dropdown-divider" />
                <li className="dropdown-header">Appointments</li>
                <li>
                  <NavLink className="dropdown-item" to="/appointments">
                    All Appointments
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/appointments/new">
                    Create Appointment
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/service-history">
                    Service History
                  </NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="/#"
                id="navbarDropdownInventory"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Inventory
              </a>
              <ul
                className="dropdown-menu"
                aria-labelledby="navbarDropdownInventory"
              >
                <li className="dropdown-header">Manufacturer</li>
                <li>
                  <NavLink className="dropdown-item" to="/manufacturers">
                    All Manufacturers
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/manufacturers/new">
                    Add Manufacturer
                  </NavLink>
                </li>
                <li className="dropdown-divider" />
                <li className="dropdown-header">Vehicles</li>
                <li>
                  <NavLink className="dropdown-item" to="/vehicle-models">
                    Vehicle Models
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/vehicle-models/new">
                    Add Vehicle Model
                  </NavLink>
                </li>
                <li className="dropdown-divider" />
                <li className="dropdown-header">Automobiles</li>
                <li>
                  <NavLink className="dropdown-item" to="/automobiles">
                    Automobiles
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/automobiles/new">
                    Add New Automobile
                  </NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="/#"
                id="navbarDropdownSales"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Sales
              </a>
              <ul
                className="dropdown-menu"
                aria-labelledby="navbarDropdownSales"
              >
                <li className="dropdown-header">Salespeople</li>
                <li>
                  <NavLink className="dropdown-item" to="/salespeople/">
                    Salespeople
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/salespeople/new/">
                    Add Salesperson
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/salespeople/history/">
                    Salesperson History
                  </NavLink>
                </li>
                <li className="dropdown-divider" />
                <li className="dropdown-header">Customers</li>
                <li>
                  <NavLink className="dropdown-item" to="/customer/">
                    Customers
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/customer/new/">
                    Add Customer
                  </NavLink>
                </li>
                <li className="dropdown-divider" />
                <li className="dropdown-header">Sales</li>
                <li>
                  <NavLink className="dropdown-item" to="/sale/">
                    Sales
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/sale/new/">
                    New Sale
                  </NavLink>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
