import React, { useEffect, useState } from "react";

const ListTechnicians = () => {
  const [technicians, setTechnicians] = useState([]);

  useEffect(() => {
    const fetchTechnicians = async () => {
      try {
        const response = await fetch("http://localhost:8080/api/technicians/");
        if (!response.ok) throw new Error("Failed to fetch technicians");
        const data = await response.json();
        setTechnicians(data.technicians);
      } catch (error) {
        console.error(error.message);
      }
    };
    fetchTechnicians();
  }, []);

  return (
    <div className="container mt-4">
      <h2>Technicians</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th scope="col">Employee ID</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
          </tr>
        </thead>
        <tbody>
          {technicians.map((technician, index) =>
            <tr key={`${technician.employee_id}-${index}`}>
              <td>
                {technician.employee_id}
              </td>
              <td>
                {technician.first_name}
              </td>
              <td>
                {technician.last_name}
              </td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  );
};

export default ListTechnicians;
