import React, { useState } from "react";

const AddTechnician = () => {
  const [technician, setTechnician] = useState({
    first_name: "",
    last_name: "",
    employee_id: ""
  });
  const [showToast, setShowToast] = useState(false);
  const [toastMessage, setToastMessage] = useState("");

  const handleChange = e => {
    setTechnician({ ...technician, [e.target.name]: e.target.value });
  };

  const handleSubmit = async e => {
    e.preventDefault();
    try {
      const response = await fetch("http://localhost:8080/api/technicians/", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(technician)
      });
      if (!response.ok) throw new Error("Failed to add technician");
      setTechnician({ first_name: "", last_name: "", employee_id: "" });
      setToastMessage("Technician Added!");
      setShowToast(true);
    } catch (error) {
      setToastMessage(error.message);
      setShowToast(true);
    }
  };

  return (
    <div className="container mt-5">
      <h2>Add a Technician</h2>
      <form onSubmit={handleSubmit} className="mt-3">
        {/* Form Fields */}
        <div className="mb-3">
          <label htmlFor="first_name" className="form-label">
            First Name:
          </label>
          <input
            type="text"
            className="form-control"
            id="first_name"
            name="first_name"
            value={technician.first_name}
            onChange={handleChange}
            required
          />
        </div>
        <div className="mb-3">
          <label htmlFor="last_name" className="form-label">
            Last Name:
          </label>
          <input
            type="text"
            className="form-control"
            id="last_name"
            name="last_name"
            value={technician.last_name}
            onChange={handleChange}
            required
          />
        </div>
        <div className="mb-3">
          <label htmlFor="employee_id" className="form-label">
            Employee ID:
          </label>
          <input
            type="text"
            className="form-control"
            id="employee_id"
            name="employee_id"
            value={technician.employee_id}
            onChange={handleChange}
            required
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Create
        </button>
      </form>

      {showToast &&
        <div
          className="toast-container position-fixed top-50 start-50 translate-middle"
          style={{ zIndex: 1050 }}
        >
          <div
            className="toast show"
            role="alert"
            aria-live="assertive"
            aria-atomic="true"
          >
            <div className="toast-header bg-dark text-white">
              <strong className="me-auto">Notification</strong>
              <button
                type="button"
                className="btn-close btn-close-white"
                data-bs-dismiss="toast"
                aria-label="Close"
                onClick={() => setShowToast(false)}
              />
            </div>
            <div className="toast-body bg-white text-dark">
              {toastMessage}
              <div className="mt-2 pt-2 border-top">
                <button
                  type="button"
                  className="btn btn-dark me-2 btn-sm"
                  onClick={() => (window.location.href = "/technicians")}
                >
                  See All Technicians
                </button>
                <button
                  type="button"
                  className="btn btn-secondary btn-sm"
                  data-bs-dismiss="toast"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>}
    </div>
  );
};

export default AddTechnician;
