import React, { useEffect, useState } from "react";

const ListAutomobiles = () => {
  const [automobiles, setAutomobiles] = useState([]);

  useEffect(() => {
    const fetchAutomobiles = async () => {
      try {
        const response = await fetch(
          "http://localhost:8100/api/automobiles/"
        );
        if (!response.ok) throw new Error("Failed to fetch automobiles");
        const data = await response.json();
        setAutomobiles(data.autos);
      } catch (error) {
        console.error(error.message);
      }
    };
    fetchAutomobiles();
  }, []);

  return (
    <div className="container mt-4">
      <h2>Automobiles</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th scope="col">VIN</th>
            <th scope="col">Color</th>
            <th scope="col">Year</th>
            <th scope="col">Model</th>
            <th scope="col">Manufacturer</th>
            <th scope="col">Sold</th>
          </tr>
        </thead>
        <tbody>
          {automobiles.map((automobile) =>
            <tr key={automobile.id}>
              <td>
                {automobile.vin}
              </td>
              <td>
                {automobile.color}
              </td>
              <td>
                {automobile.year}
              </td>
              <td>
                {automobile.model.name}
              </td>
              <td>
                {automobile.model.manufacturer.name}
              </td>
              <td>
              {automobile.sold ? "True" : "False"}
              </td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  );
};

export default ListAutomobiles;
