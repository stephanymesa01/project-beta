import React, { useEffect, useState } from "react";

const ListManufacturers = () => {
  const [manufacturers, setManufacturers] = useState([]);

  useEffect(() => {
    const fetchManufacturers = async () => {
      try {
        const response = await fetch(
          "http://localhost:8100/api/manufacturers/"
        );
        if (!response.ok) throw new Error("Failed to fetch manufacturers");
        const data = await response.json();
        setManufacturers(data.manufacturers);
      } catch (error) {
        console.error(error.message);
      }
    };
    fetchManufacturers();
  }, []);

  return (
    <div className="container mt-4">
      <h2>Manufacturers</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.map((manufacturer, index) =>
            <tr key={`${manufacturer.id}-${index}`}>
              <td>
                {manufacturer.id}
              </td>
              <td>
                {manufacturer.name}
              </td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  );
};

export default ListManufacturers;
