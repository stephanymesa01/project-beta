import React, { useState } from "react";

const AddManufacturer = () => {
  const [manufacturer, setManufacturer] = useState({
    name: ""
  });
  const [showToast, setShowToast] = useState(false);
  const [toastMessage, setToastMessage] = useState("");

  const handleChange = e => {
    setManufacturer({ ...manufacturer, [e.target.name]: e.target.value });
  };

  const handleSubmit = async e => {
    e.preventDefault();
    try {
      const response = await fetch("http://localhost:8100/api/manufacturers/", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(manufacturer)
      });
      if (!response.ok) throw new Error("Failed to add manufacturer");
      setManufacturer({ name: "" });
      setToastMessage("Manufacturer added successfully");
      setShowToast(true);
    } catch (error) {
      setToastMessage(error.message);
      setShowToast(true);
    }
  };

  return (
    <div className="container mt-5">
      <h2>Add a Manufacturer</h2>
      <form onSubmit={handleSubmit} className="mt-3">
        <div className="mb-3">
          <label htmlFor="name" className="form-label">
            Manufacturer Name:
          </label>
          <input
            type="text"
            className="form-control"
            id="name"
            name="name"
            value={manufacturer.name}
            onChange={handleChange}
            required
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Create
        </button>
      </form>

      {showToast &&
        <div
          className="toast-container position-fixed top-50 start-50 translate-middle"
          style={{ zIndex: 1050 }}
        >
          <div
            className="toast show"
            role="alert"
            aria-live="assertive"
            aria-atomic="true"
          >
            <div className="toast-header bg-dark text-white">
              <strong className="me-auto">Notification</strong>
              <button
                type="button"
                className="btn-close btn-close-white"
                data-bs-dismiss="toast"
                aria-label="Close"
                onClick={() => setShowToast(false)}
              />
            </div>
            <div className="toast-body bg-white text-dark">
              {toastMessage}
              <button
                type="button"
                className="btn btn-primary mt-2"
                onClick={() => (window.location.href = "/manufacturers")}
              >
                See All Manufacturers
              </button>
            </div>
          </div>
        </div>}
    </div>
  );
};

export default AddManufacturer;
