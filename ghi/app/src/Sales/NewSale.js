import React, { useEffect, useState } from 'react';


function NewSaleForm() {
    const [automobiles, setAutomobiles] = useState([])

    const [salespersons, setSalespersons] = useState([])

    const [customers, setCustomers] = useState([])

    const [automobile, setAutomobile] = useState('');
    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    };

    const [salesperson, setSalesperson] = useState('');
    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    };

    const [customer, setCustomer] = useState('');
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    };

    const [price, setPrice] = useState('');
    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    };



    const handleSubmit = async (event) => {
        event.preventDefault();

        const status = {sold: true}

        const data = {};
        data.automobile = automobile;
        data.salesperson = salesperson;
        data.customer = customer;
        data.price = price;

        const salesUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const InventoryAutomobileUrl = `http://localhost:8100/api/automobiles/${automobile}/`;
        const putConfig = {
            method: "put",
            body: JSON.stringify(status),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const inventoryResponse = await fetch(InventoryAutomobileUrl, putConfig)
        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok && inventoryResponse.ok) {
            setAutomobile('');
            setSalesperson('');
            setCustomer('');
            setPrice('');
        }
    }

    const fetchData = async () => {

        const AutomobileUrl = 'http://localhost:8100/api/automobiles/';
        const CustomersUrl = 'http://localhost:8090/api/customers/'
        const SalespersonUrl = 'http://localhost:8090/api/salespeople/'

        const AutomobileResponse = await fetch(AutomobileUrl);
        const CustomerResponse = await fetch(CustomersUrl)
        const SalespersonResponse = await fetch(SalespersonUrl)

        if (AutomobileResponse.ok && CustomerResponse.ok && SalespersonResponse.ok) {
            const AutomobileData = await AutomobileResponse.json();
            setAutomobiles(AutomobileData.autos)

            const CustomerData = await CustomerResponse.json();
            setCustomers(CustomerData.customers)

        const SalespersonData = await SalespersonResponse.json();
            setSalespersons(SalespersonData.salesperson)

    }
}

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-3">
                    <h1>Add New Sale</h1>
                    <form onSubmit={handleSubmit} id="create-sales-form">
                        <div className="mb-3">
                            <select onChange={handleAutomobileChange} required name="automobile" id="automobile" className="form-select" value={automobile}>
                            <option>
                                Choose an automobile!
                            </option>
                            {automobiles.map(automobile => { if (automobile["sold"] === false)
                                return (
                                    <option key={automobile.id} value={automobile.vin}>
                                        {automobile.vin}
                                    </option>
                                )
                            })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleSalespersonChange} required name="salesperson" id="salesperson" className="form-select" value={salesperson} >
                            <option>
                                Choose a Salesperson!
                            </option>
                            {salespersons.map(salesperson => {
                                return (
                                    <option key={salesperson.employee_id} value={salesperson.employee_id}>
                                        {salesperson.employee_id}
                                    </option>
                                )
                            })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleCustomerChange} required name="customer" id="customer" className="form-select" value={customer} >
                            <option>
                                Choose a Customer's Phone Number!
                            </option>
                            {customers.map(customer => {
                                return (
                                    <option key={customer.phone_number} value={customer.phone_number}>
                                        {customer.phone_number}
                                    </option>
                                )
                            })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePriceChange} placeholder="price" required type="number" name="price" id="price" className="form-control" value={price} />
                            <label htmlFor="price">Price</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}


export default NewSaleForm;
