import React, { useEffect, useState } from "react";
import NewCustomerForm from "./../Sales/NewCustomer";

const ListCustomers = () => {
  const [customers, setCustomer] = useState([]);

  useEffect(() => {
    const fetchCustomer = async () => {
      try {
        const response = await fetch("http://localhost:8090/api/customers/");
        if (!response.ok) throw new Error("Failed to fetch customer");
        const data = await response.json();
        setCustomer(data.customers);
      } catch (error) {
        console.error(error.message);
      }
    };
    fetchCustomer();
  }, []);

  return (
    <div className="container mt-4">
      <div className="text-center">
        <h2>Customers</h2>
        {customers.length === 0
          ? <div>
              <h5>No customers in history</h5>
              <NewCustomerForm />
            </div>
          : <table className="table table-hover">
              <thead>
                <tr>
                  <th scope="col">First Name</th>
                  <th scope="col">Last Name</th>
                  <th scope="col">Phone Number</th>
                  <th scope="col">Address</th>
                </tr>
              </thead>
              <tbody>
                {customers.map(customer =>
                  <tr key={customer.id}>
                    <td>
                      {customer.first_name}
                    </td>
                    <td>
                      {customer.last_name}
                    </td>
                    <td>
                      {customer.phone_number}
                    </td>
                    <td>
                      {customer.address}
                    </td>
                  </tr>
                )}
              </tbody>
            </table>}
      </div>
    </div>
  );
};

export default ListCustomers;
