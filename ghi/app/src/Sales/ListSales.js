import React, { useEffect, useState } from "react";
import NewSaleForm from "./../Sales/NewSale";

const ListSales = () => {
  const [sales, setSales] = useState([]);

  useEffect(() => {
    const fetchSales = async () => {
      try {
        const response = await fetch("http://localhost:8090/api/sales/");
        if (!response.ok) throw new Error("Failed to fetch sales");
        const data = await response.json();
        setSales(data.sales);
      } catch (error) {
        console.error(error.message);
      }
    };
    fetchSales();
  }, []);

  return (
    <div className="container mt-4">
      <div className="text-center">
        <h2>SALES</h2>
        {sales.length === 0
          ? <div>
              <h5>No sales in history</h5>
              <NewSaleForm />
            </div>
          : <table className="table table-hover">
              <thead>
                <tr>
                  <th scope="col">Salesperson Employee ID</th>
                  <th scope="col">Salesperson Name</th>
                  <th scope="col">Customer</th>
                  <th scope="col">VIN</th>
                  <th scope="col">Price</th>
                </tr>
              </thead>
              <tbody>
                {sales.map(sale =>
                  <tr key={sale.id}>
                    <td>
                      {sale.salesperson.employee_id}
                    </td>
                    <td>
                      {sale.salesperson.first_name}
                    </td>
                    <td>
                      {sale.customer.first_name}
                    </td>
                    <td>
                      {sale.automobile.vin}
                    </td>
                    <td>
                      {sale.price}
                    </td>
                  </tr>
                )}
              </tbody>
            </table>}
      </div>
    </div>
  );
};

export default ListSales;
